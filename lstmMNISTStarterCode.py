import tensorflow as tf 
from tensorflow.python.ops import rnn, rnn_cell
import numpy as np 

from tensorflow.examples.tutorials.mnist import input_data

result_dir = './results_rnn/'
mnist = input_data.read_data_sets('MNIST_data', one_hot=True) #call mnist function

learningRate = 0.1 
trainingIters = 120000
#trainingIters = 5500 * 50
batchSize = 50 
displayStep = 100

nInput = 28 #we want the input to take the 28 pixels
nSteps = 28 #every 28
nHidden = 50 #number of neurons for the RNN
nClasses = 10 #this is MNIST so you know

x = tf.placeholder('float', [None, nSteps, nInput])
y = tf.placeholder('float', [None, nClasses])

weights = {
	'out': tf.Variable(tf.random_normal([nHidden, nClasses]))
}

biases = {
	'out': tf.Variable(tf.random_normal([nClasses]))
}

def RNN(x, weights, biases):
	x = tf.transpose(x, [1,0,2])
	x = tf.reshape(x, [-1, nInput])
	x = tf.split(0, nSteps, x) #configuring so you can get it as needed for the 28 pixels

	# basic rnn cell
#	rnnCell = rnn_cell.BasicRNNCell(nHidden)

	# lstm cell
#	rnnCell = rnn_cell.BasicLSTMCell(nHidden)
#	rnnCell = rnn_cell.LSTMCell(nHidden)
	#lstmCell = #find which lstm to use in the documentation

	# gru cell
	rnnCell = rnn_cell.GRUCell(nHidden)

	outputs, states = rnn.rnn(rnnCell, x, dtype = "float") #for the rnn where to get the output and hidden state 

	return tf.matmul(outputs[-1], weights['out'])+ biases['out']

pred = RNN(x, weights, biases)

#optimization
#create the cost, optimization, evaluation, and accuracy
#for the cost softmax_cross_entropy_with_logits seems really good
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y)) 
optimizer = tf.train.AdamOptimizer(learning_rate=learningRate).minimize(cost)

correctPred = tf.equal(tf.argmax(pred,1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correctPred, tf.float32))

init = tf.initialize_all_variables()

with tf.Session() as sess:
	summary_writer = tf.train.SummaryWriter(result_dir, sess.graph)
	sess.run(init)
	step = 1
	testData = mnist.test.images.reshape((-1, nSteps, nInput))
	testLabel = mnist.test.labels


	while step* batchSize < trainingIters:
		batchX, batchY = mnist.train.next_batch(batchSize) #mnist has a way to get the next batch
		batchX = batchX.reshape((batchSize, nSteps, nInput))

		sess.run(optimizer, feed_dict={x: batchX, y: batchY})

		if step % displayStep == 0:
			train_acc_summary = tf.scalar_summary("train_accuracy", accuracy)
			loss_summary = tf.scalar_summary("loss_accuracy", cost)
			test_acc_summary = tf.scalar_summary("test_accuracy", accuracy)

			acc, train_acc_summary, loss, loss_summary = sess.run([accuracy, train_acc_summary, cost, loss_summary], feed_dict={x: batchX, y: batchY})
			#loss = sess.run(cost, feed_dict={x: batchX, y: batchY})
			test_acc, test_acc_summary = sess.run([accuracy, test_acc_summary], feed_dict={x: testData, y: testLabel})
			print("Iter " + str(step*batchSize) + ", Minibatch Loss= " + \
                  "{:.6f}".format(loss) + ", Training Accuracy= " + \
                  "{:.5f}".format(acc) + ", Test Accuracy= " + \
		  "{:.5f}".format(test_acc))
			summary_writer.add_summary(train_acc_summary, step)
			summary_writer.add_summary(loss_summary, step)
			summary_writer.add_summary(test_acc_summary, step)
			summary_writer.flush()
		step +=1
	print('Optimization finished')

	print("Testing Accuracy:", \
			sess.run(accuracy, feed_dict={x: testData, y: testLabel}))
