#%matplotlib inline
from scipy import misc
import numpy as np
import tensorflow as tf
import random
import matplotlib as mp
#set matplotlib inline
import time
import os
import shutil
# Force matplotlib to not use any Xwindows backend.
mp.use('Agg')
import matplotlib.pyplot as plt
#plt.ion()

# --------------------------------------------------
# setup

def weight_variable(shape):
    '''
    Initialize weights
    :param shape: shape of weights, e.g. [w, h ,Cin, Cout] where
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters
    Cout: the number of filters
    :return: a tensor variable for weights with initial values
    '''

    # IMPLEMENT YOUR WEIGHT_VARIABLE HERE
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)
#    return W

def bias_variable(shape):
    '''
    Initialize biases
    :param shape: shape of biases, e.g. [Cout] where
    Cout: the number of filters
    :return: a tensor variable for biases with initial values
    '''

    # IMPLEMENT YOUR BIAS_VARIABLE HERE
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)
#    return b

def conv2d(x, W):
    '''
    Perform 2-D convolution
    :param x: input tensor of size [N, W, H, Cin] where
    N: the number of images
    W: width of images
    H: height of images
    Cin: the number of channels of images
    :param W: weight tensor [w, h, Cin, Cout]
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters = the number of channels of images
    Cout: the number of filters
    :return: a tensor of features extracted by the filters, a.k.a. the results after convolution
    '''

    # IMPLEMENT YOUR CONV2D HERE
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')
#    return h_conv

def max_pool_2x2(x):
    '''
    Perform non-overlapping 2-D maxpooling on 2x2 regions in the input data
    :param x: input data
    :return: the results of maxpooling (max-marginalized + downsampling)
    '''

    # IMPLEMENT YOUR MAX_POOL_2X2 HERE
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
    #return h_max

def variable_summaries(var, name):
  """Attach a lot of summaries to a Tensor."""
  with tf.name_scope('summaries'):
    mean = tf.reduce_mean(var)
    tf.scalar_summary('mean/' + name, mean)
    with tf.name_scope('stddev'):
      stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
    tf.scalar_summary('sttdev/' + name, stddev)
    tf.scalar_summary('max/' + name, tf.reduce_max(var))
    tf.scalar_summary('min/' + name, tf.reduce_min(var))
    tf.histogram_summary(name, var)

# function to help drawing images
def put_kernels_on_grid (kernel, grid_Y, grid_X, pad = 1):

    '''Visualize conv. features as an image (mostly for the 1st layer).
    Place kernel into a grid, with some paddings between adjacent filters.

    Args:
      kernel:            tensor of shape [Y, X, NumChannels, NumKernels]
      (grid_Y, grid_X):  shape of the grid. Require: NumKernels == grid_Y * grid_X
                           User is responsible of how to break into two multiples.
      pad:               number of black pixels around each filter (between them)

    Return:
      Tensor of shape [(Y+2*pad)*grid_Y, (X+2*pad)*grid_X, NumChannels, 1].
    '''

    x_min = tf.reduce_min(kernel)
    x_max = tf.reduce_max(kernel)

    kernel1 = (kernel - x_min) / (x_max - x_min)

    # pad X and Y
    x1 = tf.pad(kernel1, tf.constant( [[pad,pad],[pad, pad],[0,0],[0,0]] ), mode = 'CONSTANT')

    # X and Y dimensions, w.r.t. padding
    Y = kernel1.get_shape()[0] + 2 * pad
    X = kernel1.get_shape()[1] + 2 * pad

    channels = kernel1.get_shape()[2]

    # put NumKernels to the 1st dimension
    x2 = tf.transpose(x1, (3, 0, 1, 2))
    # organize grid on Y axis
#    print grid_X
#    print Y * grid_Y
#    print X
#    print channels
    x3 = tf.reshape(x2, tf.pack([grid_X, Y * grid_Y, X, channels])) #3

    # switch X and Y axes
    x4 = tf.transpose(x3, (0, 2, 1, 3))
    # organize grid on X axis
    x5 = tf.reshape(x4, tf.pack([1, X * grid_X, Y * grid_Y, channels])) #3

    # back to normal order (not combining with the next step for clarity)
    x6 = tf.transpose(x5, (2, 1, 3, 0))

    # to tf.image_summary order [batch_size, height, width, channels],
    #   where in this case batch_size == 1
    x7 = tf.transpose(x6, (3, 0, 1, 2))

    # scale to [0, 255] and convert to uint8
    return tf.image.convert_image_dtype(x7, dtype = tf.uint8) 

def getActivations(layer):
    units = layer.eval(session=sess,feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 1.0})
    plotNNFilter(units)


def plotNNFilter(units):
    filters = units.shape[3]
    plt.figure(1, figsize=(20,20))
    for i in xrange(0,5):
        plt.subplot(7,6,i+1)
        plt.title('Filter ' + str(i))
        plt.imshow(units[0,:,:,i], interpolation="nearest", cmap="gray")
        #plt.show(units[0,:,:,i], interpolation="nearest", cmap="gray")

## start of the program
result_dir = './results/'
kk = 0
while 1:
	if os.path.isdir("./results_" + str(kk)):
		kk = kk + 1
	else:
		result_dir = './results_' + str(kk)
		break
max_step = 5 # the maximum iterations. After max_step iterations, the training will stop no matter what

start_time = time.time() # start timing

ntrain = 1000 # per class
ntest = 100 # per class
nclass =  10 # number of classes
imsize = 28
nchannels = 1 
batchsize = 50

Train = np.zeros((ntrain*nclass,imsize,imsize,nchannels))
Test = np.zeros((ntest*nclass,imsize,imsize,nchannels))
LTrain = np.zeros((ntrain*nclass,nclass))
LTest = np.zeros((ntest*nclass,nclass))

itrain = -1
itest = -1
for iclass in range(0, nclass):
    for isample in range(0, ntrain):
        path = './CIFAR10/Train/%d/Image%05d.png' % (iclass,isample)
        im = misc.imread(path); # 28 by 28
        im = im.astype(float)/255
        itrain += 1
        Train[itrain,:,:,0] = im
        LTrain[itrain,iclass] = 1 # 1-hot lable
    for isample in range(0, ntest):
        path = './CIFAR10/Test/%d/Image%05d.png' % (iclass,isample)
        im = misc.imread(path); # 28 by 28
        im = im.astype(float)/255
        itest += 1
        Test[itest,:,:,0] = im
        LTest[itest,iclass] = 1 # 1-hot lable

nsamples = itrain + 1
sess = tf.InteractiveSession()

tf_data = tf.placeholder(tf.float32, shape=[None, 28, 28, 1]) #tf variable for the data, remember shape is [None, width, height, numberOfChannels] 
tf_labels = tf.placeholder(tf.float32, shape=[None, 10]) #tf variable for labels

# reshape the input image
# tf_image = tf.reshape(tf_data, [-1, 28, 28, 1])

# --------------------------------------------------
# model
#create your model

# first convolutional layer
W_conv1 = weight_variable([5, 5, 1, 32])
#variable_summaries(W_conv1, "layer1" + '/weights')
b_conv1 = bias_variable([32])
#variable_summaries(b_conv1, "layer1" + '/biases')
#variable_summaries(x_image, "layer1" + '/inputs')
h_conv1 = tf.nn.relu(conv2d(tf_data, W_conv1) + b_conv1)
#variable_summaries(h_conv1, "layer1" + '/activation after relu')
h_pool1 = max_pool_2x2(h_conv1)
variable_summaries(h_pool1, "layer1" + '/activation after max pooling')

# second convolutional layer
W_conv2 = weight_variable([5, 5, 32, 64])
#variable_summaries(W_conv2, "layer2" + '/weights')
b_conv2 = bias_variable([64])
#variable_summaries(b_conv2, "layer2" + '/biases')
#variable_summaries(h_pool1, "layer2" + '/inputs')
h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
#variable_summaries(h_conv2, "layer2" + '/activation after relu')
h_pool2 = max_pool_2x2(h_conv2)
variable_summaries(h_pool2, "layer2" + '/activation after max pooling')

# densely connected layer
W_fc1 = weight_variable([7 * 7 * 64, 1024])
#variable_summaries(W_fc1, "dense connected layer" + '/weights')
b_fc1 = bias_variable([1024])
#variable_summaries(b_fc1, "dense connected layer" + '/biases')
h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
#variable_summaries(h_pool2_flat, "dense connected layer" + '/inputs')
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)
#variable_summaries(h_fc1, "dense connected layer" + '/activation after relu')

# densely connected layer2
#W_fc2 = weight_variable([1024, 10])
#b_fc2 = bias_variable([10])
#h_fc2 = tf.nn.relu(tf.matmul(h_fc1, W_fc2) + b_fc2)

# dropout
keep_prob = tf.placeholder(tf.float32)
#h_fc2_drop = tf.nn.dropout(h_fc2, keep_prob)
h_fc2_drop = tf.nn.dropout(h_fc1, keep_prob)

# softmax
#W_fc3 = weight_variable([10, 10])
W_fc3 = weight_variable([1024, 10])
#variable_summaries(W_fc2, "softmax layer" + '/weights')
b_fc3 = bias_variable([10])
#variable_summaries(b_fc2, "softmax layer" + '/biases')
#variable_summaries(h_fc1_drop, "softmax layer" + '/inputs')
y_conv = tf.nn.softmax(tf.matmul(h_fc2_drop, W_fc3) + b_fc3)
#y_conv = tf.nn.softmax(tf.matmul(h_fc2_drop, W_fc3) + b_fc3)

# --------------------------------------------------
# loss
#set up the loss, optimization, evaluation, and accuracy

# setup training
cross_entropy = tf.reduce_mean(-tf.reduce_sum(tf_labels * tf.log(y_conv), reduction_indices=[1]))
train_step = tf.train.AdamOptimizer(1e-3).minimize(cross_entropy)
#train_step = tf.train.FtrlOptimizer(1e-4).minimize(cross_entropy)
#train_step = tf.train.RMSPropOptimizer(1e-4).minimize(cross_entropy)
#train_step = tf.train.MomentumOptimizer(1e-3, 0.001).minimize(cross_entropy)
#train_step = tf.train.GradientDescentOptimizer(1e-3).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(tf_labels,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# Add a scalar summary for the snapshot loss.
#tf.scalar_summary(cross_entropy.op.name, cross_entropy)
tf.scalar_summary("loss", cross_entropy)
# Add a scalar summary for the for the accuracy
tf.scalar_summary('train accuracy', accuracy)
# Build the summary operation based on the TF collection of Summaries.
summary_op = tf.merge_all_summaries()

# Create a saver for writing training checkpoints.
saver = tf.train.Saver()

# Instantiate a SummaryWriter to output summaries and the Graph.
summary_writer = tf.train.SummaryWriter(result_dir, sess.graph)

# --------------------------------------------------
# optimization

sess.run(tf.initialize_all_variables())
batch_xs = np.zeros((batchsize,imsize,imsize,nchannels)) #setup as [batchsize, width, height, numberOfChannels] and use np.zeros()
batch_ys = np.zeros((batchsize, nclass)) #setup as [batchsize, the how many classes] 
for i in range(max_step): # try a small iteration size once it works then continue
    perm = np.arange(nsamples)
    np.random.shuffle(perm)
    for j in range(batchsize):
        batch_xs[j,:,:,:] = Train[perm[j],:,:,:]
        batch_ys[j,:] = LTrain[perm[j],:]
    if i%100 == 0:
        # calculate train accuracy and print it
    	# output the training accuracy every 10 iterations
#    	train_accuracy = accuracy.eval(feed_dict={
#		tf_data:batch_xs, tf_labels:batch_ys, keep_prob: 1.0})
#	train_accuracy_summary = tf.scalar_summary('train accuracy', train_accuracy)

    	# Update the events file which is used to monitor the training (in this case,
    	# only the training loss is monitored)
    	summary_str, train_accuracy = sess.run([summary_op, accuracy], feed_dict={tf_data:batch_xs, tf_labels:batch_ys, keep_prob: 1.0})
    	summary_writer.add_summary(summary_str, i)
#    	summary_writer.add_summary(train_accuracy_summary, i)
    	summary_writer.flush()

        checkpoint_file = os.path.join(result_dir, 'checkpoint')
    	saver.save(sess, checkpoint_file, global_step=i)

#    	test_accuracy = accuracy.eval(feed_dict={
#		tf_data: Test, tf_labels: LTest, keep_prob: 1.0})

    	test_summary = tf.scalar_summary('test accuracy', accuracy)

    	test_summary, test_accuracy = sess.run([test_summary, accuracy], feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 1.0})
    	summary_writer.add_summary(test_summary, i)
    	summary_writer.flush()

	print("step %d, training accuracy %g, test_accuracy %g"%(i, train_accuracy, test_accuracy))

    train_step.run(feed_dict={tf_data:batch_xs, tf_labels:batch_ys, keep_prob: 0.5})
#    optimizer.run(feed_dict={}) # dropout only during training

# --------------------------------------------------
# test

print("test accuracy %g"%accuracy.eval(feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 1.0}))

stop_time = time.time()
print('The training takes %f second to finish'%(stop_time - start_time))

# visualization

grid_x = 6   # to get a square grid for 64 conv1 features
grid_y = 6
#weights = tf.get_variable(W_conv1)
#weights = W_conv1
W1_a = W_conv1                            # [5, 5, 3, 32]
W1pad= tf.zeros([5, 5, 1, 4])        # [5, 5, 3, 4]  - four zero kernels for padding
weights = tf.concat(3, [W1_a, W1pad])   # [5, 5, 3, 36]
grid = put_kernels_on_grid (weights, grid_y, grid_x, 1)
weight_summary = tf.image_summary('conv1/features', grid, max_images=1)

#grid1 = put_kernels_on_grid (h_pool1, grid_y, grid_x, 1)
#act_summary = tf.image_summary('conv1/activation', grid1, max_images=1)


"""
weights = h_pool1
x_min = tf.reduce_min(weights)
x_max = tf.reduce_max(weights)
weights_0_to_1 = (weights - x_min) / (x_max - x_min)
weights_0_to_255_uint8 = tf.image.convert_image_dtype (weights_0_to_1, dtype=tf.uint8)

# to tf.image_summary format [batch_size, height, width, channels]
weights_transposed = tf.transpose (weights_0_to_255_uint8, [3, 0, 1, 2])

# this will display random 3 filters from the 64 in conv1
#weight_summary = tf.image_summary('conv1/filters', weights_transposed, max_images=32)
act_summary = tf.image_summary('conv1/activations', weights_transposed, max_images=3)
"""

"""
W1_a = W_conv1                            # [5, 5, 3, 32]
W1pad= tf.zeros([5, 5, 1, 4])        # [5, 5, 3, 4]  - four zero kernels for padding
W1_b = tf.concat(3, [W1_a, W1pad])   # [5, 5, 3, 36]
W1_c = tf.split(3, 36, W1_b)         # 36 x [5, 5, 3, 1]
W1_row0 = tf.concat(0, W1_c[0:6])    # [30, 5, 3, 1]
W1_row1 = tf.concat(0, W1_c[6:12])   # [30, 5, 3, 1]
W1_row2 = tf.concat(0, W1_c[12:18])  # [30, 5, 3, 1]
W1_row3 = tf.concat(0, W1_c[18:24])  # [30, 5, 3, 1]
W1_row4 = tf.concat(0, W1_c[24:30])  # [30, 5, 3, 1]
W1_row5 = tf.concat(0, W1_c[30:36])  # [30, 5, 3, 1]
W1_d = tf.concat(1, [W1_row0, W1_row1, W1_row2, W1_row3, W1_row4, W1_row5]) # [30, 30, 3, 1]
W1_e = tf.reshape(W1_d, [1, 30, 30, 1])
#Wtag = tf.placeholder(tf.string, None)
weight_summary = tf.image_summary('conv1/filters', W1_e)
"""


#weight_summary = tf.image_summary('conv1/filters', W_conv1, max_images=32)
weight_summary = sess.run(weight_summary)
#act_summary = sess.run(act_summary, feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 1.0})
summary_writer.add_summary(weight_summary)
#summary_writer.add_summary(act_summary)
summary_writer.flush()

#getActivations(h_conv1)

sess.close()
